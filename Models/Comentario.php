<?php
/**
 * Created by PhpStorm.
 * User: GIO
 * Date: 10/11/2019
 * Time: 09:09 AM
 */

namespace Clase3\Models;


class Comentario extends Conexion
{
    public $usuario_id;
    public $publicacion_id;
    public $fecha_hora;
    public $contenido;

    static function all()
    {
        $me = new Conexion();
        $pre = mysqli_prepare($me->con, "SELECT * FROM comentarios");
        $pre->execute();
        $res = $pre->get_result();

        $comentarios = [];
        while ($comentario = $res->fetch_object(Comentario::class)){
            array_push($comentarios, $comentario);
        }


        return $comentarios;
    }
}